package com.org.greatlearning.assessment.week3;
import java.util.*;
public class Main {

	public static void main(String[] args) {
		Scanner sc =new Scanner(System.in);
	   MagicOfBooks obj=new MagicOfBooks();
	   
		while(true) {
			System.out.println("1. add entry into book\n"
					+ "2.delete entries from book\n" + "3.update a book\n"
					+ "4. display all the books \n" + "5. total count of all the books\n"
					+ "6.search for autobiography books" +  "\n 7.display by features");
       
			System.out.println("enter your choice according to you:");
			int choice = sc.nextInt();

			switch (choice) {

			case 1:          //Add a book of your choice
				System.out.println("Please Enter Number of Books you want to add");
				int n=sc.nextInt();
				
				for(int i=1;i<=n;i++) {
					obj.addbook();
					}
				break;
			
			case 2:      //Delete a book of your choice
				
				   obj.deletebook();
				    break;
	
			case 3:       //Update the book which you want to update
				obj.updatebook();
				break;

			case 4:       //Display the book which you want
				obj.displayBookInfo();
				break;
			
			case 5:       //counting the books 
				System.out.println("Count of all books-");
				obj.count();
break;
				
			case 6:        //As an Admin,I can see all the books under Autobiography genre
				obj.autobiography();
				break;
			      
			
			case 7:
				System.out.println("Enter your choice:\n 1. Price low to high "
						+ "\n 2.Price high to low \n 3. Best selling");
				int ch = sc.nextInt();

				switch (ch) {

				case 1 : obj.displayByFeature(1);
				         break;
				case 2:obj.displayByFeature(2); 
				         break;
				case 3:obj.displayByFeature(3);
				        break;
				}
			
			default:
				System.out.println("Yoy have entered wrong choice!!!!!");

			}

		}
      
	}
}


